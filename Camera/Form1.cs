﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace Camera
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }

        private FilterInfoCollection device;
        private VideoCaptureDevice image;
        private void Form1_Load(object sender, EventArgs e)
        {
            device = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo capture in device)
            {
                comboBox1.Items.Add(capture.Name);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = @"c:\picture";
            if(saveFileDialog1.ShowDialog()== DialogResult.OK)
            {
                pictureBox1.Image.Save(saveFileDialog1.FileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (image.IsRunning)
            {
                image.Stop();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            image = new VideoCaptureDevice(device[comboBox1.SelectedIndex].MonikerString);
            image.NewFrame += new NewFrameEventHandler(cam);
            image.Start();


        }
        void cam(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bit = (Bitmap)eventArgs.Frame.Clone();
            pictureBox1.Image = bit;
        }
    }
}
